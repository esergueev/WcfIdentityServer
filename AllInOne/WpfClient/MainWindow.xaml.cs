﻿using IdentityModel.Client;
using System;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using System.ServiceModel;
using System.Windows;
using System.Xml.Linq;
using Constants;
using IdentityModel.Constants;
using IdentityModel.Extensions;
using Thinktecture.Samples;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        LoginWebView _login;
        AuthorizeResponse _response;

        public MainWindow()
        {
            InitializeComponent();

            _login = new LoginWebView();
            _login.Done += _login_Done;

            Loaded += MainWindow_Loaded;
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _login.Owner = this;
        }

        void _login_Done(object sender, AuthorizeResponse e)
        {
            _response = e;
            Textbox1.Text = e.Raw;
        }

       

        private void RequestToken(string scope, string responseType)
        {
            var request = new AuthorizeRequest(Urls.AuthorizeEndpoint);
            var startUrl = request.CreateAuthorizeUrl(
                clientId: "wpf.webview.client",
                responseType: responseType,
                responseMode: "form_post",
                scope: scope,
                redirectUri: "oob://localhost/wpf.webview.client",
                state: "random_state",
                nonce: "random_nonce" /**,
                loginHint: "alice",
                acrValues: "idp:Google b c" **/);

            _login.Show();
            _login.Start(new Uri(startUrl), new Uri("oob://localhost/wpf.webview.client"));
        }


        static GenericXmlSecurityToken WrapJwt(string jwt)
        {
            var subject = new ClaimsIdentity("saml");
            subject.AddClaim(new Claim("jwt", jwt));

            var descriptor = new SecurityTokenDescriptor
            {
                TokenType = TokenTypes.Saml2TokenProfile11,
                TokenIssuerName = "urn:wrappedjwt",
                Subject = subject
            };

            var handler = new Saml2SecurityTokenHandler();
            var token = handler.CreateToken(descriptor);

            var xmlToken = new GenericXmlSecurityToken(
                XElement.Parse(token.ToTokenXmlString()).ToXmlElement(),
                null,
                DateTime.Now,
                DateTime.Now.AddHours(1),
                null,
                null,
                null);

            return xmlToken;
        }

        private void CallUserInfo_Click(object sender, RoutedEventArgs e)
        {
            // authorization header
            if (_response != null && _response.Values.ContainsKey("access_token"))
            {
                var xmlToken = WrapJwt(_response.AccessToken);

                var binding =
                    new WS2007FederationHttpBinding(WSFederationHttpSecurityMode.TransportWithMessageCredential);
                binding.HostNameComparisonMode = HostNameComparisonMode.Exact;
                binding.Security.Message.EstablishSecurityContext = false;
                binding.Security.Message.IssuedKeyType = SecurityKeyType.BearerKey;

                //Переделать по феншую - Closing the proxy and using statement книга Programming WCF Services by Juval Lowy.
                using (var factory = new ChannelFactory<IPingPongService>(
                    binding,
                    new EndpointAddress(Urls.WcfPing)))
                {
                    var channel = factory.CreateChannelWithIssuedToken(xmlToken);
                    Textbox1.Text = channel.Pong();
                }
            }
        }

        private void AccessTokenOnlyButton_Click(object sender, RoutedEventArgs e)
        {
            RequestToken("write", "token");
        }
    }
}