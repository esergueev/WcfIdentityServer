﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IdentityModel.Configuration;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using Constants;
using Microsoft.IdentityModel.Protocols;


namespace WCF.PongService.Host
{
    class Program
    {
        static void Main(string[] args)
        {
            var host = new ServiceHost(
                typeof (PingPongService),
                new Uri(Constants.Urls.WcfServiceEndpoint));

            host.Credentials.IdentityConfiguration = CreateIdentityConfiguration();
            host.Credentials.UseIdentityConfiguration = true;

            var authz = host.Description.Behaviors.Find<ServiceAuthorizationBehavior>();
            authz.PrincipalPermissionMode = PrincipalPermissionMode.Always;

            host.AddServiceEndpoint(typeof (IPingPongService), CreateBinding(), "ping");

            host.Open();

            Console.WriteLine("server running...");
            Console.ReadLine();

            host.Close();
        }


        static IdentityConfiguration CreateIdentityConfiguration()
        {
            var identityConfiguration = new IdentityConfiguration();

            identityConfiguration.SecurityTokenHandlers.Clear();
            identityConfiguration.SecurityTokenHandlers.Add(
                new IdentityServerWrappedJwtHandler(Constants.Urls.BaseAddress, "write"));
            identityConfiguration.ClaimsAuthorizationManager = new RequireAuthenticationAuthorizationManager();

            return identityConfiguration;
        }

        static Binding CreateBinding()
        {
            var binding = new WS2007FederationHttpBinding(WSFederationHttpSecurityMode.TransportWithMessageCredential);

            // only for testing on localhost
            binding.HostNameComparisonMode = HostNameComparisonMode.Exact;

            binding.Security.Message.EstablishSecurityContext = false;
            binding.Security.Message.IssuedKeyType = SecurityKeyType.BearerKey;

            return binding;
        }
    }


    class IdentityServerWrappedJwtHandler : Saml2SecurityTokenHandler
    {
        X509Certificate2 _signingCert;
        string _issuerName;
        private readonly string[] _requiredScopes;

        public IdentityServerWrappedJwtHandler(string authority, params string[] requiredScopes)
        {
            JwtSecurityTokenHandler.InboundClaimTypeMap = new Dictionary<string, string>();

            var discoveryEndpoint = authority.EnsureTrailingSlash() + Urls.OpenIdConfiguration;
            var configurationManager = new ConfigurationManager<OpenIdConnectConfiguration>(discoveryEndpoint);

            var config = configurationManager.GetConfigurationAsync().Result;
            _signingCert = new X509Certificate2(Convert.FromBase64String(config.JsonWebKeySet.Keys.First().X5c.First()));
            _issuerName = config.Issuer;
            _requiredScopes = requiredScopes;
        }

        public override ReadOnlyCollection<ClaimsIdentity> ValidateToken(SecurityToken token)
        {
            var saml = token as Saml2SecurityToken;
            var samlAttributeStatement = saml.Assertion.Statements.OfType<Saml2AttributeStatement>().FirstOrDefault();
            var jwt =
                samlAttributeStatement.Attributes.Where(sa => sa.Name.Equals("jwt", StringComparison.OrdinalIgnoreCase))
                    .SingleOrDefault()
                    .Values.Single();

            var parameters = new TokenValidationParameters
            {
                ValidAudience = _issuerName.EnsureTrailingSlash() + "resources",
                ValidIssuer = _issuerName,
                IssuerSigningToken = new X509SecurityToken(_signingCert)
            };

            SecurityToken validatedToken;
            var handler = new JwtSecurityTokenHandler();
            var principal = handler.ValidateToken(jwt, parameters, out validatedToken);

            var ci = new ReadOnlyCollection<ClaimsIdentity>(new List<ClaimsIdentity> {principal.Identities.First()});

            if (_requiredScopes.Any())
            {
                bool found = false;

                foreach (var scope in _requiredScopes)
                {
                    if (principal.HasClaim("scope", scope))
                    {
                        found = true;
                        break;
                    }
                }

                if (found == false)
                {
                    throw new SecurityTokenValidationException("Insufficient Scope");
                }
            }

            return ci;
        }
    }

    class RequireAuthenticationAuthorizationManager : ClaimsAuthorizationManager
    {
        public override bool CheckAccess(AuthorizationContext context)
        {
            return context.Principal.Identity.IsAuthenticated;
        }
    }

    internal static class StringExtensions
    {
        public static string EnsureTrailingSlash(this string input)
        {
            if (!input.EndsWith("/"))
            {
                return input + "/";
            }

            return input;
        }
    }
}