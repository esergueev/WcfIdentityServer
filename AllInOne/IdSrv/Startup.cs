﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using IdentityServer3.Core;
using IdentityServer3.Core.Configuration;
using IdentityServer3.Core.Models;
using IdentityServer3.Core.Services.InMemory;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof (IdSrv.Startup))]

namespace IdSrv
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var factory = new IdentityServerServiceFactory()
                .UseInMemoryUsers(Users.Get())
                .UseInMemoryClients(Clients.Get())
                .UseInMemoryScopes(Scopes.Get());

            var options = new IdentityServerOptions
            {
                SigningCertificate = Certificate.Load(),
                Factory = factory,
            };

            app.UseIdentityServer(options);
        }
    }

    public class Certificate
    {
        public static X509Certificate2 Load()
        {
            var assembly = typeof (Certificate).Assembly;
            using (var stream = assembly.GetManifestResourceStream("IdSrv.idsrv3test.pfx"))
            {
                return new X509Certificate2(ReadStream(stream), "idsrv3test");
            }
        }

        private static byte[] ReadStream(Stream input)
        {
            var buffer = new byte[16*1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }

    public class Scopes
    {
        public static IEnumerable<Scope> Get()
        {
            yield return StandardScopes.OpenId;
            yield return StandardScopes.Profile;
            yield return StandardScopes.Email;
            yield return StandardScopes.Address;
            yield return StandardScopes.AllClaims;

            yield return new Scope
            {
                Name = "write",
                DisplayName = "Write data",
                Type = ScopeType.Resource,
                Emphasize = true,
                ScopeSecrets = new List<Secret>
                {
                    new Secret("secret".Sha256())
                }
            };
        }
    }

    public class Clients
    {
        public static IEnumerable<Client> Get()
        {
            /////////////////////////////////////////////////////////////
            // WPF WebView Client Sample
            /////////////////////////////////////////////////////////////
            yield return
                new Client
                {
                    ClientName = "WPF WebView Client Sample",
                    ClientId = "wpf.webview.client",
                    Flow = Flows.Implicit,
                    AllowedScopes = new List<string>
                    {
                        Constants.StandardScopes.OpenId,
                        Constants.StandardScopes.Profile,
                        Constants.StandardScopes.Email,
                        Constants.StandardScopes.Roles,
                        Constants.StandardScopes.Address,
                        "read",
                        "write"
                    },
                    ClientUri = "https://identityserver.io",
                    RequireConsent = true,
                    AllowRememberConsent = true,
                    RedirectUris = new List<string>
                    {
                        "oob://localhost/wpf.webview.client",
                    },
                };
        }
    }

    public class Users
    {
        public static List<InMemoryUser> Get()
        {
            return GetUsers().ToList();
        }

        private static IEnumerable<InMemoryUser> GetUsers()
        {
            yield return new InMemoryUser
            {
                Subject = "88421113",
                Username = "bob",
                Password = "bob",
                Claims = new Claim[]
                {
                    new Claim(Constants.ClaimTypes.Name, "Bob Smith"),
                    new Claim(Constants.ClaimTypes.GivenName, "Bob"),
                    new Claim(Constants.ClaimTypes.FamilyName, "Smith"),
                    new Claim(Constants.ClaimTypes.Email, "BobSmith@email.com"),
                    new Claim(Constants.ClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean),
                    new Claim(Constants.ClaimTypes.Role, "Developer"),
                    new Claim(Constants.ClaimTypes.Role, "Geek"),
                    new Claim(Constants.ClaimTypes.WebSite, "http://bob.com"),
                    new Claim(Constants.ClaimTypes.Address,
                        @"{ ""street_address"": ""One Hacker Way"", ""locality"": ""Heidelberg"", ""postal_code"": 69118, ""country"": ""Germany"" }",
                        Constants.ClaimValueTypes.Json)
                }
            };
        }
    }
}