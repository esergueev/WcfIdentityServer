﻿using System.Security.Claims;
using System.ServiceModel;
using System.Text;
using Constants;

namespace WCF.PongService.Host
{
   

    public class PingPongService: IPingPongService
    {
        public string Pong()
        {
            var sb = new StringBuilder();

            foreach (var claim in ClaimsPrincipal.Current.Claims)
            {
                sb.AppendFormat("{0} :: {1}\n", claim.Type, claim.Value);
            }

            return sb.ToString();
        }
    }
}