﻿using System.ServiceModel;

namespace Constants
{
    public static class Urls
    {
        public const string BaseAddress = "https://localhost:44310/";

        public const string AuthorizeEndpoint = BaseAddress + "/connect/authorize";
        public const string OpenIdConfiguration = ".well-known/openid-configuration";


        public const string WcfServiceEndpoint = "https://localhost:44311/";
        public const string WcfPing = WcfServiceEndpoint + "ping";

    }

    [ServiceContract]
    public interface IPingPongService
    {
        [OperationContract]
        string Pong();
    }
}